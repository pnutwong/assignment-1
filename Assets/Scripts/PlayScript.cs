﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayScript : MonoBehaviour {

  public void PlayButton()
    {
        SceneManager.LoadScene("gameplay");
    }

  public void QuitButton()
  {
      Application.Quit();
  }
}
