﻿using UnityEngine;
using System.Collections;

public class mole : MonoBehaviour {

    Animator moleAnimation;
    bool hit;
    public float waitTime;
    public GameObject hole;
    AudioSource sound;

	// Use this for initialization
	void Start () {
        moleAnimation = transform.GetComponent<Animator>();
        sound = transform.GetComponent<AudioSource>();
        StartCoroutine("triggered");
	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}

    void OnMouseDown()
    {
        if(hit == false)
        {
            if (sound.isPlaying == false)
            {
                sound.Play();
            }
            moleAnimation.SetBool("Continue", true);
            moleAnimation.SetBool("Hit", true);
            hit = true;
        }
    }

    IEnumerator triggered()
    {
        yield return new WaitForSeconds(waitTime);
        moleAnimation.SetBool("Continue", true);
        moleAnimation.SetBool("Hit", false);
    }

    public void OnHit()
    {
        Camera.main.transform.GetComponent<Points>().score += 1;
        delete();
    }

    public void delete()
    {
        hole.GetComponent<Hole>().done = true;
        Destroy(gameObject);
    }

    public void NotHit()
    {
        Camera.main.transform.GetComponent<Points>().lives -= 1;
        delete();
    }
}
