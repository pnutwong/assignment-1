﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Points : MonoBehaviour {

    public int lives;
    public int score;
    public Text LiveText;
    public Text ScoreText;
    public GameObject hole;
    public int numberOfHoles;
	// Use this for initialization
	void Start () {

        for (int i = 0; i < numberOfHoles; i++)
        {
            Instantiate(hole, new Vector3(Random.Range(-4f, 4f), Random.Range(-3f, 3f), 0), Quaternion.identity);
        }
	}
	
	// Update is called once per frame
	void Update () {
        ScoreText.text = score.ToString();
        LiveText.text = lives.ToString();

        Lose();
        Win();
	}

    public void Lose()
    {
        if (lives <= 0)
        {
            SceneManager.LoadScene("Losemenu");
        }
    }

    public void Win()
    {
        if (score >= 20)
        {
            SceneManager.LoadScene("Winmenu");
        }
    }
}
